# transaction-service

Microservicio de gestión de transacciones.

### Cuentas existentes:
IBAN  | SALDO DISPONIBLE
------------- | -------------
ES9820385778983000760236 | 5000
ES9820385778983000760302 | 400
ES9820385778983000765555 | 1850.21
### URL de pruebas:
URL | DESCRIPCION
--- | ---
http://transaction-service.microdev.me/h2-console/ | H2 CONSOLE
http://transaction-service.microdev.me/swagger-ui.html | SWAGGER UI

### Compilar Jar
`mvn package`

### Ejecutar pruebas
`mvn test`

### Ejecutar microservicio
`java -jar app_release.jar`







