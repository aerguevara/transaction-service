package com.banking.utils;

import lombok.Getter;

@Getter
public enum TransactionStatus {
    PENDING("PENDING"),
    SETTLED("SETTLED"),
    FUTURE("FUTURE"),
    INVALID("INVALID");

    private String value;

    TransactionStatus(String value) {
        this.value = value;
    }
}
