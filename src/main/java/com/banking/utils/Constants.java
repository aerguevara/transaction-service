package com.banking.utils;

public final class Constants {
    public static final String ASC = "asc";
    public static final String DESC = "desc";
    public static final String SUFIX_REFERENCE = "OB";

    private Constants() {
        throw new IllegalStateException("Utility class");
    }

}
