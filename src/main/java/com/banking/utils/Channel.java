package com.banking.utils;

import lombok.Getter;

@Getter
public enum Channel {
    CLIENT("CLIENT"),
    ATM("ATM"),
    INTERNAL("INTERNAL");


    private String value;

    Channel(String value) {
        this.value = value;
    }
}
