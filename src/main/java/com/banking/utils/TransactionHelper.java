package com.banking.utils;

import com.banking.transaction.model.dto.TransactionDTO;
import com.banking.transaction.model.dto.TransactionStatusRSDTO;
import com.banking.transaction.model.entity.Transaction;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static com.banking.utils.Constants.SUFIX_REFERENCE;

public final class TransactionHelper {

    private TransactionHelper() {
        throw new IllegalStateException("Utility class");
    }

    public static BigDecimal substractFeeAmount(BigDecimal amount, BigDecimal fee) {
        if (fee != null) {
            if (amount.signum() < 0) {
                amount = amount.add(fee);
            } else {
                amount = amount.subtract(fee);
            }
        }
        return amount;
    }

    public static void setFieldValueDefault(TransactionDTO transactionDTO) {
        if (transactionDTO.getDate() == null) {
            transactionDTO.setDate(LocalDateTime.now());
        }
        if (StringUtils.isEmpty(transactionDTO.getReference())) {
            transactionDTO.setReference(System.currentTimeMillis() + SUFIX_REFERENCE);
        }
    }

    public static void setterFeeAndAmount(TransactionStatusRSDTO transactionStatusRSDTO,
                                          Transaction transaction,
                                          Channel channel) {
        if (Channel.INTERNAL.equals(channel)) {
            transactionStatusRSDTO.setAmount(transaction.getAmount());
            transactionStatusRSDTO.setFee(transaction.getFee());
        } else {
            transactionStatusRSDTO.setAmount(
                    substractFeeAmount(transaction.getAmount(), transaction.getFee()));
        }
    }
}
