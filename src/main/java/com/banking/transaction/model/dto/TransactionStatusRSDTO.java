package com.banking.transaction.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TransactionStatusRSDTO {

    private String reference;
    private String status;
    private BigDecimal amount;
    private BigDecimal fee;

    public TransactionStatusRSDTO(String reference, String status) {
        this.reference = reference;
        this.status = status;
    }
}
