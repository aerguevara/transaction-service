package com.banking.transaction.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class TransactionStatusRQDTO {

    @NotEmpty
    private String reference;
    private String channel;
}
