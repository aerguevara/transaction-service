package com.banking.transaction.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "ACCOUNT")
@Getter
@Setter
public class Account {

    @Id
    @Column(name = "ID")
    @GeneratedValue
    private Long id;
    @Column(name = "IBAN")
    private String iban;
    @Column(name = "AMOUNT_AVAILABLE")
    private BigDecimal amountAvailable;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "UPDATE_DATE")
    private LocalDateTime updatedDate;
}
