package com.banking.transaction.service;

import com.banking.transaction.model.entity.Account;
import com.banking.transaction.repository.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Optional<Account> findAccountByIban(String iban) {
        return accountRepository.findAccountByIban(iban);
    }

    @Override
    public void updateAccount(Account account) {
        accountRepository.save(account);
    }
}
