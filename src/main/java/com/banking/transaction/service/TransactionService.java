package com.banking.transaction.service;

import com.banking.transaction.model.dto.TransactionDTO;
import com.banking.transaction.model.dto.TransactionStatusRQDTO;
import com.banking.transaction.model.dto.TransactionStatusRSDTO;

import java.util.List;

public interface TransactionService {

    void saveTransaction(TransactionDTO transactionDTO);

    List<TransactionDTO> findByIbanOrderBy(String iban, String orderBy, String direction);

    TransactionStatusRSDTO findbyTransactionStatus(TransactionStatusRQDTO transactionStatusRQDTO);


}
