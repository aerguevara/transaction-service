package com.banking.transaction.service;

import com.banking.transaction.model.entity.Account;

import java.util.Optional;

public interface AccountService {

    Optional<Account> findAccountByIban(String iban);

    void updateAccount(Account account);
}
