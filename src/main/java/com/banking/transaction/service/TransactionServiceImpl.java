package com.banking.transaction.service;

import com.banking.transaction.config.MicroException;
import com.banking.transaction.model.dto.TransactionDTO;

import com.banking.transaction.model.dto.TransactionStatusRQDTO;
import com.banking.transaction.model.dto.TransactionStatusRSDTO;
import com.banking.transaction.model.entity.Account;
import com.banking.transaction.model.entity.Transaction;
import com.banking.transaction.repository.TransactionRepository;
import com.banking.utils.Channel;
import com.banking.utils.Constants;
import com.banking.utils.TransactionStatus;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static com.banking.utils.TransactionHelper.*;
import static java.util.stream.Collectors.toList;

@Service
public class TransactionServiceImpl implements TransactionService {


    private AccountService accountService;
    private TransactionRepository transactionRepository;

    public TransactionServiceImpl(AccountService accountService,
                                  TransactionRepository transactionRepository) {
        this.accountService = accountService;
        this.transactionRepository = transactionRepository;
    }

    @Override
    public void saveTransaction(TransactionDTO transactionDTO) {
        Optional<Account> accountOptional = accountService.findAccountByIban(transactionDTO.getIban());
        setFieldValueDefault(transactionDTO);
        if (accountOptional.isPresent()) {
            updatedAccountAmountAvailable(transactionDTO, accountOptional.get());
            Transaction transaction = new Transaction();
            BeanUtils.copyProperties(transactionDTO, transaction);
            transactionRepository.save(transaction);
        } else {
            throw new MicroException("Account Not Found",
                    "No se encontro la cuenta del IBAN proporcionado",
                    HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public List<TransactionDTO> findByIbanOrderBy(String iban, String orderBy, String direction) {
        Sort sort;
        if (direction.equalsIgnoreCase(Constants.ASC)) {
            sort = Sort.by(Sort.Direction.ASC, orderBy);
        } else if (direction.equalsIgnoreCase(Constants.DESC)) {
            sort = Sort.by(Sort.Direction.DESC, orderBy);
        } else {
            throw new MicroException("direction not supported",
                    "direction not supported",
                    HttpStatus.BAD_REQUEST);
        }
        return transactionRepository.findByIban(iban, sort)
                .stream()
                .map(TransactionDTO::new)
                .collect(toList());
    }

    @Override
    public TransactionStatusRSDTO findbyTransactionStatus(TransactionStatusRQDTO transactionStatusRQDTO) {
        Optional<Transaction> transaction =
                transactionRepository.findByReference(transactionStatusRQDTO.getReference());
        return transaction
                .map(transactionTransform(transactionStatusRQDTO))
                .orElseGet(() ->
                        new TransactionStatusRSDTO(transactionStatusRQDTO.getReference(),
                                TransactionStatus.INVALID.getValue()));


    }

    private Function<Transaction, TransactionStatusRSDTO> transactionTransform(
            TransactionStatusRQDTO transactionStatusRQDTO) {
        return transactionFound -> {
            Channel channel = Channel.valueOf(transactionStatusRQDTO.getChannel());
            return buildResponseTransactionStatus(transactionFound, channel);
        };
    }

    private TransactionStatusRSDTO buildResponseTransactionStatus(Transaction transaction, Channel channel) {
        TransactionStatusRSDTO transactionStatusRSDTO = new TransactionStatusRSDTO();
        transactionStatusRSDTO.setReference(transaction.getReference());
        setterFeeAndAmount(transactionStatusRSDTO, transaction, channel);
        LocalDate transactionDate = transaction.getDate().toLocalDate();
        if (transactionDate.isBefore(LocalDate.now())) {
            transactionStatusRSDTO.setStatus(TransactionStatus.SETTLED.getValue());
        } else if (transactionDate.isEqual(LocalDate.now()) || Channel.ATM.equals(channel)) {
            transactionStatusRSDTO.setStatus(TransactionStatus.PENDING.getValue());
        } else {
            transactionStatusRSDTO.setStatus(TransactionStatus.FUTURE.getValue());
        }
        return transactionStatusRSDTO;
    }


    private void updatedAccountAmountAvailable(TransactionDTO transactionDTO, Account account) {
        BigDecimal amountReal = substractFeeAmount(transactionDTO.getAmount(), transactionDTO.getFee());
        BigDecimal result = account.getAmountAvailable().add(amountReal);
        if (transactionDTO.getAmount().signum() < 0) {
            if (result.signum() >= 0) {
                updateAccountAmount(account, result);
            } else {
                throw new MicroException("Transaction Error",
                        "La cuenta no puede quedar con saldo negativo",
                        HttpStatus.BAD_REQUEST);
            }
        } else {
            updateAccountAmount(account, result);
        }
    }


    private void updateAccountAmount(Account account, BigDecimal result) {
        account.setUpdatedDate(LocalDateTime.now());
        account.setAmountAvailable(result);
        accountService.updateAccount(account);
    }

}
