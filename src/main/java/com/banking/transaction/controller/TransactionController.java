package com.banking.transaction.controller;

import com.banking.transaction.model.dto.TransactionDTO;
import com.banking.transaction.model.dto.TransactionStatusRQDTO;
import com.banking.transaction.model.dto.TransactionStatusRSDTO;
import com.banking.transaction.service.TransactionService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(TransactionController.PATH)
public class TransactionController {

    public static final String PATH = "transaction";
    public static final String PATH_SAVE_TRANSACTION = "/register";
    public static final String PATH_TRANSACTION_STATUS = "/status";


    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping(PATH_SAVE_TRANSACTION)
    public void saveTransaction(@RequestBody @Valid TransactionDTO transactionDTO) {
        transactionService.saveTransaction(transactionDTO);

    }

    @GetMapping
    public List<TransactionDTO> findByIbanOrderBy(@RequestParam String iban,
                                                  @RequestParam String orderBy,
                                                  @RequestParam String direction) {
        return transactionService.findByIbanOrderBy(iban, orderBy, direction);
    }

    @PostMapping(PATH_TRANSACTION_STATUS)
    TransactionStatusRSDTO findbyTransactionStatus(@RequestBody @Valid TransactionStatusRQDTO transactionStatusRQDTO) {
        return transactionService.findbyTransactionStatus(transactionStatusRQDTO);
    }
}
