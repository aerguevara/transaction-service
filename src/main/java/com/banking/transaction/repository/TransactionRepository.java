package com.banking.transaction.repository;

import com.banking.transaction.model.entity.Transaction;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    List<Transaction> findByIban(String iban, Sort sort);

    Optional<Transaction> findByReference(String reference);
}
