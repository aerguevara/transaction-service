package com.banking.transaction.config;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class MicroException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private HttpStatus status;
    private String name;
    private String errorMessage;


    public MicroException(String name, String errorMessage, HttpStatus httpStatus) {
        super(errorMessage);
        this.name = name;
        this.errorMessage = errorMessage;
        this.status = httpStatus;
    }

    public MicroException(Exception cause) {
        super(cause);
        this.name = cause.getClass().getName();
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.errorMessage = cause.getMessage();
    }
}
