package com.banking.transaction.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ErrorResponse {

    private int status;
    private String name;
    private String message;
    private long timestamp;
    private Map<String, String> details;

    public ErrorResponse(MicroException e) {
        this.status = e.getStatus().value();
        this.name = e.getName();
        this.message = e.getErrorMessage();
        timestamp = System.currentTimeMillis();
    }

}
