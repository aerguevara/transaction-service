package com.banking.transaction.service;


import com.banking.transaction.config.MicroException;
import com.banking.transaction.model.dto.TransactionDTO;
import com.banking.transaction.model.dto.TransactionStatusRQDTO;
import com.banking.transaction.model.dto.TransactionStatusRSDTO;
import com.banking.transaction.model.entity.Account;
import com.banking.transaction.model.entity.Transaction;
import com.banking.transaction.repository.TransactionRepository;
import com.banking.transaction.service.AccountService;
import com.banking.transaction.service.TransactionServiceImpl;
import com.banking.utils.Channel;
import com.banking.utils.Constants;
import com.banking.utils.TransactionStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
public class TransactionServiceTest {

    private static final String IBAN = "ES9820385778983000760302";
    private static final BigDecimal TRANSACTION_AMOUNT_CREDIT = BigDecimal.valueOf(500);
    private static final BigDecimal ACCOUNT_AMOUNT = BigDecimal.valueOf(500);
    private static final BigDecimal TRANSACTION_AMOUNT_DEBIT = BigDecimal.valueOf(-600);
    private static final BigDecimal COMISSION = BigDecimal.valueOf(3.15);
    public static final String ANY_STRING = "ANY_STRING";

    @InjectMocks
    private TransactionServiceImpl transactionService;

    @Mock
    private AccountService accountService;

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private Account account;

    @Mock
    private Transaction transaction;


    @Test
    public void saveTransactionTest() {
        TransactionDTO transactionDTO = createMockTransaction();
        transactionDTO.setAmount(TRANSACTION_AMOUNT_CREDIT);
        transactionDTO.setFee(COMISSION);
        transactionDTO.setIban(IBAN);
        Mockito.when(account.getAmountAvailable()).thenReturn(ACCOUNT_AMOUNT);
        Mockito.when(accountService.findAccountByIban(Mockito.anyString())).thenReturn(Optional.of(account));
        transactionService.saveTransaction(transactionDTO);
        Mockito.verify(transactionRepository).save(Mockito.any(Transaction.class));
        Mockito.verify(accountService).updateAccount(Mockito.any(Account.class));
    }

    @Test(expected = MicroException.class)
    public void saveTransactionDebitAccountNegativeExceptionTest() {
        TransactionDTO transactionDTO = createMockTransaction();
        transactionDTO.setAmount(TRANSACTION_AMOUNT_DEBIT);
        transactionDTO.setFee(COMISSION);
        transactionDTO.setIban(IBAN);
        Mockito.when(account.getAmountAvailable()).thenReturn(ACCOUNT_AMOUNT);
        Mockito.when(accountService.findAccountByIban(Mockito.anyString())).thenReturn(Optional.of(account));
        transactionService.saveTransaction(transactionDTO);
    }

    @Test(expected = MicroException.class)
    public void saveTransactionNotFoundTest() {
        TransactionDTO transactionDTO = createMockTransaction();
        Mockito.when(accountService.findAccountByIban(Mockito.anyString())).thenReturn(Optional.empty());
        transactionService.saveTransaction(transactionDTO);
    }

    @Test
    public void findByIbanOrderBySuccessTest() {
        Mockito.when(transactionRepository.findByIban(Mockito.anyString(), Mockito.any(Sort.class)))
                .thenReturn(Collections.singletonList(transaction));
        transactionService.findByIbanOrderBy(IBAN, "amount", Constants.ASC);
        Mockito.verify(transactionRepository).findByIban(Mockito.anyString(), Mockito.any(Sort.class));
    }

    @Test(expected = MicroException.class)
    public void findByIbanOrderByFailedTest() {
        Mockito.when(transactionRepository.findByIban(Mockito.anyString(), Mockito.any(Sort.class)))
                .thenReturn(Collections.singletonList(transaction));
        transactionService.findByIbanOrderBy(IBAN, "amount", ANY_STRING);
    }

    @Test
    public void businessRulesA() {
        TransactionStatusRQDTO transactionStatusRQDTO = buildTransactionRQDTO();
        transactionStatusRQDTO.setChannel(Channel.ATM.getValue());
        Mockito.when(transactionRepository.findByReference(Mockito.anyString())).thenReturn(Optional.empty());
        transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        TransactionStatusRSDTO transactionStatusRSDTO =
                transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        assertEquals(TransactionStatus.INVALID.getValue(), transactionStatusRSDTO.getStatus());
    }

    @Test
    public void businessRulesBClient() {
        TransactionStatusRQDTO transactionStatusRQDTO = buildTransactionRQDTO();
        transactionStatusRQDTO.setChannel(Channel.CLIENT.getValue());
        mockGetterTransactionAmount();
        Mockito.when(transaction.getDate())
                .thenReturn(LocalDateTime.now().minusDays(1));
        Mockito.when(transactionRepository.findByReference(Mockito.anyString()))
                .thenReturn(Optional.of(transaction));
        transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        TransactionStatusRSDTO transactionStatusRSDTO =
                transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        assertEquals(TransactionStatus.SETTLED.getValue(), transactionStatusRSDTO.getStatus());
    }

    @Test
    public void businessRulesC() {
        TransactionStatusRQDTO transactionStatusRQDTO = buildTransactionRQDTO();
        transactionStatusRQDTO.setChannel(Channel.INTERNAL.getValue());
        mockGetterTransactionAmount();
        Mockito.when(transaction.getDate())
                .thenReturn(LocalDateTime.now().minusDays(1));
        Mockito.when(transactionRepository.findByReference(Mockito.anyString()))
                .thenReturn(Optional.of(transaction));
        transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        TransactionStatusRSDTO transactionStatusRSDTO =
                transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        assertEquals(TransactionStatus.SETTLED.getValue(), transactionStatusRSDTO.getStatus());
    }

    @Test
    public void businessRulesE() {
        TransactionStatusRQDTO transactionStatusRQDTO = buildTransactionRQDTO();
        transactionStatusRQDTO.setChannel(Channel.INTERNAL.getValue());
        mockGetterTransactionAmount();
        Mockito.when(transaction.getDate())
                .thenReturn(LocalDateTime.now());
        Mockito.when(transactionRepository.findByReference(Mockito.anyString()))
                .thenReturn(Optional.of(transaction));
        transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        TransactionStatusRSDTO transactionStatusRSDTO =
                transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        assertEquals(TransactionStatus.PENDING.getValue(), transactionStatusRSDTO.getStatus());
    }

    @Test
    public void businessRulesf() {
        TransactionStatusRQDTO transactionStatusRQDTO = buildTransactionRQDTO();
        transactionStatusRQDTO.setChannel(Channel.CLIENT.getValue());
        mockGetterTransactionAmount();
        Mockito.when(transaction.getDate())
                .thenReturn(LocalDateTime.now().plusDays(1));
        Mockito.when(transactionRepository.findByReference(Mockito.anyString()))
                .thenReturn(Optional.of(transaction));
        transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        TransactionStatusRSDTO transactionStatusRSDTO =
                transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        assertEquals(TransactionStatus.FUTURE.getValue(), transactionStatusRSDTO.getStatus());
    }

    @Test
    public void businessRulesG() {
        TransactionStatusRQDTO transactionStatusRQDTO = buildTransactionRQDTO();
        transactionStatusRQDTO.setChannel(Channel.ATM.getValue());
        mockGetterTransactionAmount();
        Mockito.when(transaction.getDate())
                .thenReturn(LocalDateTime.now().plusDays(1));
        Mockito.when(transactionRepository.findByReference(Mockito.anyString()))
                .thenReturn(Optional.of(transaction));
        transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        TransactionStatusRSDTO transactionStatusRSDTO =
                transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        assertEquals(TransactionStatus.PENDING.getValue(), transactionStatusRSDTO.getStatus());
    }

    @Test
    public void businessRulesH() {
        TransactionStatusRQDTO transactionStatusRQDTO = buildTransactionRQDTO();
        transactionStatusRQDTO.setChannel(Channel.INTERNAL.getValue());
        mockGetterTransactionAmount();
        Mockito.when(transaction.getDate())
                .thenReturn(LocalDateTime.now().plusDays(1));
        Mockito.when(transactionRepository.findByReference(Mockito.anyString()))
                .thenReturn(Optional.of(transaction));
        transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        TransactionStatusRSDTO transactionStatusRSDTO =
                transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        assertEquals(TransactionStatus.FUTURE.getValue(), transactionStatusRSDTO.getStatus());
    }

    @Test
    public void businessRulesDClient() {
        TransactionStatusRQDTO transactionStatusRQDTO = buildTransactionRQDTO();
        transactionStatusRQDTO.setChannel(Channel.CLIENT.getValue());
        mockGetterTransactionAmount();
        Mockito.when(transaction.getDate())
                .thenReturn(LocalDateTime.now());
        Mockito.when(transactionRepository.findByReference(Mockito.anyString()))
                .thenReturn(Optional.of(transaction));
        transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        TransactionStatusRSDTO transactionStatusRSDTO =
                transactionService.findbyTransactionStatus(transactionStatusRQDTO);
        assertEquals(TransactionStatus.PENDING.getValue(), transactionStatusRSDTO.getStatus());
    }

    private void mockGetterTransactionAmount() {
        Mockito.when(transaction.getAmount()).thenReturn(TRANSACTION_AMOUNT_CREDIT);
        Mockito.when(transaction.getFee()).thenReturn(COMISSION);
    }

    private TransactionStatusRQDTO buildTransactionRQDTO() {
        TransactionStatusRQDTO transactionStatusRQDTO = new TransactionStatusRQDTO();
        transactionStatusRQDTO.setReference(ANY_STRING);
        return transactionStatusRQDTO;
    }

    private TransactionDTO createMockTransaction() {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setAmount(TRANSACTION_AMOUNT_CREDIT);
        transactionDTO.setFee(COMISSION);
        transactionDTO.setIban(IBAN);
        return transactionDTO;
    }

}
