package com.banking.transaction.cucumber.bdd.stepdefs;

import com.banking.transaction.cucumber.bdd.HelperCucumber;
import com.banking.transaction.model.dto.TransactionDTO;
import com.banking.transaction.model.dto.TransactionStatusRQDTO;
import com.banking.transaction.model.dto.TransactionStatusRSDTO;
import com.banking.utils.Channel;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class BusinessRuleC {

    public static final String REFERENCE = "505050C";

    private final RestTemplate restTemplate = new RestTemplate();

    private final TransactionStatusRQDTO transactionStatusRQDTO = new TransactionStatusRQDTO();

    private TransactionStatusRSDTO transactionStatusRSDTO;


    @Given("transaction that is stored in our system and status from INTERNAL channel And the transaction date is before today")
    public void transaction_that_is_stored_in_our_system_and_status_from_INTERNAL_channel_And_the_transaction_date_is_before_today() {
        TransactionDTO transactionDTO = HelperCucumber.getInstanceTransaction();
        transactionDTO.setDate(LocalDateTime.now().minusDays(1));
        transactionDTO.setReference(REFERENCE);
        restTemplate.postForObject(HelperCucumber.SAVE_TRANSACTION, transactionDTO, void.class);
        transactionStatusRQDTO.setReference(REFERENCE);
    }

    @When("I check the status from INTERNAL channel And the transaction date is before today")
    public void i_check_the_status_from_INTERNAL_channel_And_the_transaction_date_is_before_today() {
        transactionStatusRQDTO.setChannel(Channel.INTERNAL.getValue());

        transactionStatusRSDTO =
                restTemplate
                        .postForObject(HelperCucumber.STATUS_TRANSACTION,
                                transactionStatusRQDTO,
                                TransactionStatusRSDTO.class);
    }

    @Then("The system returns the status {string} for business rule C")
    public void The_system_returns_the_status(String expectedResultStatus) {
        assertEquals(expectedResultStatus, transactionStatusRSDTO.getStatus());
    }
}