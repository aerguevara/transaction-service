package com.banking.transaction.cucumber.bdd.stepdefs;

import com.banking.transaction.cucumber.bdd.HelperCucumber;
import com.banking.transaction.model.dto.TransactionStatusRQDTO;
import com.banking.transaction.model.dto.TransactionStatusRSDTO;
import com.banking.utils.Channel;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;

public class BusinessRuleA {

    private final RestTemplate restTemplate = new RestTemplate();

    private final TransactionStatusRQDTO transactionStatusRQDTO = new TransactionStatusRQDTO();

    private TransactionStatusRSDTO transactionStatusRSDTO;


    @Given("A transaction that is not stored in our system")
    public void a_transaction_that_is_not_stored_in_our_system() {
        transactionStatusRQDTO.setReference("00000");
    }

    @When("I check the status from any channel")
    public void I_check_the_status_from_any_channel() {

        transactionStatusRQDTO.setChannel(Channel.ATM.getValue());

        transactionStatusRSDTO =
                restTemplate
                        .postForObject(HelperCucumber.STATUS_TRANSACTION,
                                transactionStatusRQDTO,
                                TransactionStatusRSDTO.class);
    }

    @Then("The system returns the status INVALID {string}")
    public void The_system_returns_the_status(String expectedResultStatus) {
        assertEquals(expectedResultStatus, transactionStatusRSDTO.getStatus());
    }
}