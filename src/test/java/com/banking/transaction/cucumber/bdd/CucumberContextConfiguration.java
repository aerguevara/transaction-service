package com.banking.transaction.cucumber.bdd;


import com.banking.transaction.TransactionApplication;
import io.cucumber.java.Before;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;

@Slf4j
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@ContextConfiguration(classes = TransactionApplication.class, loader = SpringBootContextLoader.class)
public class CucumberContextConfiguration {

    @Before
    public void setUp() {
        log.info("*SET UP CONTEXT SPRING*");
    }
}
