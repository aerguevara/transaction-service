package com.banking.transaction.cucumber.bdd.stepdefs;

import com.banking.transaction.cucumber.bdd.HelperCucumber;
import com.banking.transaction.model.dto.TransactionDTO;
import com.banking.transaction.model.dto.TransactionStatusRQDTO;
import com.banking.transaction.model.dto.TransactionStatusRSDTO;
import com.banking.utils.Channel;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class BusinessRuleD {

    public static final String REFERENCE = "505050D";

    private final RestTemplate restTemplate = new RestTemplate();

    private final TransactionStatusRQDTO transactionStatusRQDTO = new TransactionStatusRQDTO();

    private TransactionStatusRSDTO transactionStatusRSDTO;

    @Given("A transaction that is stored in our system and status from CLIENT or ATM channel And the transaction date is equals to today")
    public void a_transaction_that_is_stored_in_our_system_and_status_from_CLIENT_or_ATM_channel_And_the_transaction_date_is_equals_to_today() {
        TransactionDTO transactionDTO = HelperCucumber.getInstanceTransaction();
        transactionDTO.setDate(LocalDateTime.now());
        transactionDTO.setReference(REFERENCE);
        restTemplate.postForObject(HelperCucumber.SAVE_TRANSACTION, transactionDTO, void.class);
        transactionStatusRQDTO.setReference(REFERENCE);
    }

    @When("I check the status from CLIENT or ATM channel And the transaction date is equals to today")
    public void i_check_the_status_from_CLIENT_or_ATM_channel_And_the_transaction_date_is_equals_to_today() {
        transactionStatusRQDTO.setChannel(Channel.CLIENT.getValue());

        transactionStatusRSDTO =
                restTemplate
                        .postForObject(HelperCucumber.STATUS_TRANSACTION,
                                transactionStatusRQDTO,
                                TransactionStatusRSDTO.class);
    }

    @Then("The system returns the status {string} for business rule D")
    public void The_system_returns_the_status(String expectedResultStatus) {
        assertEquals(expectedResultStatus, transactionStatusRSDTO.getStatus());
    }
}