package com.banking.transaction.cucumber.bdd;

import com.banking.transaction.controller.TransactionController;
import com.banking.transaction.model.dto.TransactionDTO;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public final class HelperCucumber {

    public static final String PATH_BASE = "http://localhost:8080/".concat(TransactionController.PATH);
    public static final String SAVE_TRANSACTION = PATH_BASE
            .concat(TransactionController.PATH_SAVE_TRANSACTION);
    public static final String STATUS_TRANSACTION = PATH_BASE
            .concat(TransactionController.PATH_TRANSACTION_STATUS);

    private HelperCucumber() {
        throw new IllegalStateException("Utility class");
    }

    public static TransactionDTO getInstanceTransaction() {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setIban("ES9820385778983000765555");
        transactionDTO.setAmount(BigDecimal.valueOf(500));
        transactionDTO.setDate(LocalDateTime.now());
        transactionDTO.setDescription("100 montaditos");
        transactionDTO.setFee(BigDecimal.valueOf(1.35));
        return transactionDTO;
    }
}
