package com.banking.transaction.cucumber.bdd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, plugin = { "json:target/cucumber.json"},
        features = "src/test/resources/business",
        glue = "com.banking.transaction.cucumber")
@SpringBootTest
public class TransactionApplicationTest {

}
