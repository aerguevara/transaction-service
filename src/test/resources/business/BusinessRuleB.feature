Feature: transaction that is stored in our system and status from CLIENT or ATM channel And the transaction date is before today
    
  Scenario: 
	Given A transaction that is stored in our system and status from CLIENT or ATM channel And the transaction date is before today
	When I check the status from CLIENT or ATM channel And the transaction date is before today
	Then The system returns the status "SETTLED" for business rule B
	