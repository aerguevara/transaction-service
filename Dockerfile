FROM openjdk:8-jdk-alpine
LABEL maintainer="aerguevara@gmail.com"
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE
ADD ${JAR_FILE} app_release.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app_release.jar"]

